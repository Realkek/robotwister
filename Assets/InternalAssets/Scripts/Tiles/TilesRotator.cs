﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesRotator : MonoBehaviour, IEventTrigger, IEventSub
{
    private void Start()
    {
        Subscribe();
    }

    void TileRotateNotify()
    {
        TriggerEvent(TilesEventsStore.TileRotated);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerShifted, TileRotateNotify);
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }
}