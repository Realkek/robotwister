﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour, IEventSub
{
    private bool _isMoving;
    private float _tileMoveSpeed = 7.5f;

    private void Awake()
    {
        Subscribe();
    }

    private void Update()
    {
        if (_isMoving)
            transform.Translate(Vector3.forward * _tileMoveSpeed * Time.deltaTime);
    }

    void EnableMoving()
    {
        _isMoving = true;
    }

    void DisableMoving()
    {
        _isMoving = false;
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFirstTapped, EnableMoving);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerClickMissed, DisableMoving);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFell, DisableMoving);
        ManagerEvents.StartListening(PlayerEventsStore.MoveSpeedDeBuffReceived, DecreaseMoveSpeed);
        ManagerEvents.StartListening(PlayerEventsStore.MoveSpeedBuffReceived, IncreaseMoveSpeed);
    }

    private void DecreaseMoveSpeed()
    {
        StartCoroutine(MoveSpeedDecreaser());
    }

    private void IncreaseMoveSpeed()
    {
        StartCoroutine(MoveSpeedIncreaser());
    }

    private IEnumerator MoveSpeedIncreaser()
    {
        _tileMoveSpeed += 1.5f;
        yield return new WaitForSeconds(3);
        _tileMoveSpeed -= 1.5f;
    }

    private IEnumerator MoveSpeedDecreaser()
    {
        _tileMoveSpeed -= 1.5f;
        yield return new WaitForSeconds(3);
        _tileMoveSpeed += 1.5f;
    }

    public void UnSubscribe()
    {
    }
}