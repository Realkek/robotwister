﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class TilesPlacer : MonoBehaviour, IEventSub, IEventTrigger
{
    [SerializeField] private List<GameObject> tilesPositionContainer = new List<GameObject>();
    public GameObject player;
    public GameObject tapsPanel;
    private int _startTilePositionNumber;
    private int _previousTilePositionNumber;
    [SerializeField] private List<GameObject> tilesVariations = new List<GameObject>();
    private bool _isFirstTile = true;
    private int _tilesCount = 0;
    private int _tileId;
    private GameObject _previousTile;
    private bool _isFirstMovingTile = true;
    private Dictionary<string, GameObject> _tilesRotatesDictionary = new Dictionary<string, GameObject>();
    int _randomPositionSideNumber;
    private float _newTileZRotation;
    private Vector3 _newTilePosition;

    void Start()
    {
        Subscribe();
        _startTilePositionNumber = 0;
        StartCoroutine(TileCreateAwaiter());
        StartCoroutine(PlayerActivator());
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(TilesEventsStore.TileCreated, CreateTile);
        ManagerEvents.StartListening(TilesEventsStore.TileDestroying, ActivateNextTile);
        ManagerEvents.StartListening(PlayerEventsStore.CurrentHitTileReceived, CheckReceivedTileHit);
    }

    private IEnumerator TileCreateAwaiter()
    {
        while (_tilesCount < 1000)
        {
            TriggerEvent(TilesEventsStore.TileCreated);
            TreatmentTile();
            _tilesCount++;
        }

        yield return new WaitForSeconds(1);
    }

    private IEnumerator PlayerActivator()
    {
        yield return new WaitForSeconds(1.8f);
        player.SetActive(true);
        tapsPanel.SetActive(true);
    }


    void CreateTile()
    {
        _previousTile = transform.GetChild(transform.childCount - 1).gameObject;
        _randomPositionSideNumber = Random.Range(0, 2);
        int tileVariationNumber = 0;
        if (_isFirstTile)
        {
            _newTilePosition = tilesPositionContainer[_startTilePositionNumber].transform.localPosition;
            _newTileZRotation = tilesPositionContainer[_startTilePositionNumber].transform.localEulerAngles.z;
            _isFirstTile = false;
        }
        else
        {
            tileVariationNumber = Random.Range(0, 2);
            ChooseNewPositionNumber();
            _newTilePosition = tilesPositionContainer[_startTilePositionNumber].transform.localPosition;
            _newTileZRotation = tilesPositionContainer[_startTilePositionNumber].transform.localEulerAngles.z;
        }

        if (!_isFirstMovingTile)
            _newTilePosition.z = _previousTile.transform.position.z + 7f;
        _isFirstMovingTile = false;
        _previousTile = Instantiate(tilesVariations[tileVariationNumber], _newTilePosition,
            Quaternion.Euler(0, 0, _newTileZRotation),
            transform);
    }


    void ChooseNewPositionNumber()
    {
        if (_previousTilePositionNumber == 0 || _previousTilePositionNumber == 7)
        {
            switch (_previousTilePositionNumber)
            {
                case 0:
                    if (_randomPositionSideNumber == 0)
                        _startTilePositionNumber = 1;
                    else if (_randomPositionSideNumber == 1)
                    {
                        _startTilePositionNumber = 7;
                    }

                    break;
                case 7:
                    if (_randomPositionSideNumber == 0)
                        _startTilePositionNumber = 0;
                    else if (_randomPositionSideNumber == 1)
                    {
                        _startTilePositionNumber = 6;
                    }

                    break;
            }
        }
        else
        {
            if (_randomPositionSideNumber == 0)
                _startTilePositionNumber = _previousTilePositionNumber - 1;
            else if (_randomPositionSideNumber == 1)
            {
                _startTilePositionNumber = _previousTilePositionNumber + 1;
            }
        }
    }

    void TreatmentTile()
    {
        if (_tileId > 8)
            _previousTile.SetActive(false);
        _previousTile.name = $"Tile{_tileId}";
        _tilesRotatesDictionary.Add(_previousTile.name, _previousTile);
        _previousTilePositionNumber = _startTilePositionNumber;
        _tileId++;
    }

    void ActivateNextTile()
    {
        for (int i = 1; i < transform.childCount - 1; i++)
        {
            if ((transform.GetChild(i).CompareTag("LongTile")
                 || transform.GetChild(i).CompareTag("MediumTile")
                 || transform.GetChild(i).CompareTag("ShortTile")) &&
                transform.GetChild(i).gameObject.activeSelf == false)
            {
                transform.GetChild(i).gameObject.SetActive(true);
                break;
            }
        }
    }

    void CheckReceivedTileHit<TColliderHitName>(TColliderHitName hitName)
    {
        if (hitName is string receivedRayCastHitName)
            if (_tilesRotatesDictionary.TryGetValue(receivedRayCastHitName, out var receivedTile))
            {
                for (int i = 0; i < transform.childCount - 1; i++)
                {
                    if (transform.GetChild(i).name == receivedTile.name)
                    {
                        // if (transform.GetChild(i).transform.eulerAngles.z >
                        //     transform.GetChild(i + 1).transform.eulerAngles.z)

                        ManagerEvents.CheckTriggeringEvent(PlayerEventsStore.PlayerShifted,
                            transform.GetChild(i + 1).gameObject);
                        ManagerEvents.CheckTriggeringEvent(PlayerEventsStore.PlayerJumped);
                        RotateTiles(i);
                    }
                }
            }
    }

    void RotateTiles(int i)
    {
        if (Math.Abs(transform.GetChild(i).localEulerAngles.z) < 1 &&
            Math.Abs(transform.GetChild(i + 1).localEulerAngles.z) > 314)
        {
            StartCoroutine(TilesRotateChecker(1));
        }

        else if (Math.Abs(transform.GetChild(i).localEulerAngles.z) > 314 &&
                 Math.Abs(transform.GetChild(i + 1).localEulerAngles.z) < 1)
        {
            StartCoroutine(TilesRotateChecker(0));
        }
        else if (transform.GetChild(i).localEulerAngles.z > transform.GetChild(i + 1).localEulerAngles.z)
        {
            StartCoroutine(TilesRotateChecker(1));
        }

        else if (transform.GetChild(i).localEulerAngles.z < transform.GetChild(i + 1).localEulerAngles.z)
        {
            StartCoroutine(TilesRotateChecker(0));
        }

        // transform.Rotate(0, 0, -transform.GetChild(i + 1).transform.eulerAngles.z);
    }

    private IEnumerator TilesRotateChecker(int isRight)
    {
        int i = 0;
        while (i < 270)
        {
            i += 30;
            if (isRight == 1)
                transform.Rotate(0, 0, 5);
            else if (isRight == 0)
                transform.Rotate(0, 0, -5);
            yield return 0.01f;
        }
    }


    public void UnSubscribe()
    {
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName, arguments);
    }
}