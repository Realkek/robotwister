﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TilesEventsStore
{
    public static string TileCreated = "TileCreated";
    public static string TileDestroying = "TileDestroying";
    public static string TileRotated = "TileRotated";
}