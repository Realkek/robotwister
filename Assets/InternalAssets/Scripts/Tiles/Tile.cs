﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Tile : MonoBehaviour, IEventTrigger, IEventSub
{
    private bool _isMovingUpAvailable;
    private int _movementStepCount = 0;
    public Material material;

    private void Start()
    {
        Subscribe();
        transform.position = transform.position + transform.up * -20;
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFell, EnableSubCollider);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerClickMissed, EnableSubCollider);
        ManagerEvents.StartListening(PlayerEventsStore.TilesColorChanged, ChangeTilesColor);
    }

    private void OnEnable()
    {
        _isMovingUpAvailable = true;
    }

    private void Update()
    {
        if (_isMovingUpAvailable && _movementStepCount < 100)
        {
            _movementStepCount++;
            transform.position = transform.position + transform.up * 0.2f;
        }
        else if (_movementStepCount >= 100)
            _isMovingUpAvailable = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("TilesDestroyingCollider"))
        {
            TriggerEvent(TilesEventsStore.TileDestroying);
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            TriggerEvent(PlayerEventsStore.PlayerJumpMissed);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }

    void EnableSubCollider()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }

    void ChangeTilesColor()
    {
        int randomColorNumber = Random.Range(0, 6);
        switch (randomColorNumber)
        {
            case 0:
                material.color = Color.blue;
                break;
            case 1:
                material.color = Color.cyan;
                break;
            case 2:
                material.color = Color.green;
                break;
            case 3:
                material.color = Color.magenta;
                break;
            case 4:
                material.color = Color.red;
                break;
            case 5:
                material.color = Color.yellow;
                break;
        }
    }

    public void UnSubscribe()
    {
    }
}