﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvas : MonoBehaviour, IEventSub
{
    public GameObject mainMenuPanel;
    public GameObject buttons;
    public GameObject tapToPlayText;

    private void Start()
    {
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFirstTapped, HideMainMenuCanvas);
    }

    void HideMainMenuCanvas()
    {
        mainMenuPanel.SetActive(false);
        buttons.SetActive(false);
        tapToPlayText.SetActive(false);
    }

    public void UnSubscribe()
    {
    }
}