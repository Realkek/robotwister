﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreCounterText : MonoBehaviour, IEventSub, IEventTrigger
{
    private int _score;
    private TextMeshProUGUI _textMeshPro;

    private void Start()
    {
        _textMeshPro = GetComponent<TextMeshProUGUI>();
        Subscribe();
        ShowStartScore();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerJumped, IncrementScore);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerClickMissed, HideScoreCounterText);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFell, HideScoreCounterText);
    }

    void HideScoreCounterText()
    {
        transform.gameObject.SetActive(false);
    }

    void IncrementScore()
    {
        _score++;
        _textMeshPro.text = _score.ToString();
        PlayerPrefs.SetInt("score", _score);
        CheckNewScoreCount();
        TriggerEvent(PlayerEventsStore.ScoreChanged);
    }

    void ShowStartScore()
    {
        _textMeshPro.text = _score.ToString();
        PlayerPrefs.SetInt("score", _score);
        CheckNewScoreCount();
        TriggerEvent(PlayerEventsStore.ScoreChanged);
        CheckScoreForTiles();
    }

    void CheckScoreForTiles()
    {
        if (_score > 0 && _score % 10 == 0)
            TriggerEvent(PlayerEventsStore.TilesColorChanged);
    }

    void CheckNewScoreCount()
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            int highScore = PlayerPrefs.GetInt("HighScore", _score);
            if (_score > highScore)
                PlayerPrefs.SetInt("HighScore", _score);
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", _score);
        }

        CheckScoreForTiles();
    }

    public void UnSubscribe()
    {
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}