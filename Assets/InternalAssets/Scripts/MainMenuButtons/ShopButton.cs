﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopButton : Buttons
{
    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        ResetButtonSize();
    }
}