﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour
{
    public AudioClip audioClip;
    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        CheckAudioButtonState();
        ManagerEvents.StartListening("SubSettingsButtonStateChanged", CheckAudioButtonState);
    }

    protected void IncreaseButtonSize()
    {
        transform.localScale = new Vector3(transform.localScale.x * 1.1f, transform.localScale.y * 1.1f
        );
        if (audioClip != null && _audioSource != null)
            _audioSource.Play();
    }

    protected void ResetButtonSize()
    {
        transform.localScale = new Vector3(transform.localScale.x / 1.1f, transform.localScale.y / 1.1f
        );
    }

    private void CheckAudioButtonState()
    {
        if (PlayerPrefs.GetInt("isSoundOn") == 0)
            _audioSource.mute = true;
        else if (PlayerPrefs.GetInt("isSoundOn") == 1)
        {
            _audioSource.mute = false;
        }
    }
}