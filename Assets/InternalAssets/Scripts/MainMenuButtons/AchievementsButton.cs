﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsButton : Buttons
{
    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        ResetButtonSize();
    }
}