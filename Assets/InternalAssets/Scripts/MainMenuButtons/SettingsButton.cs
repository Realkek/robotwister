﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsButton : Buttons, IEventSub
{
    private int _isMusicOn = 1;
    private int _isSoundOn = 1;
    public GameObject soundOnButton;
    public GameObject soundOffButton;
    public GameObject musicOnButton;
    public GameObject musicOffButton;
    private bool _isActiveSubButtons;

    private void Start()
    {
        Subscribe();
    }

    void SetButtonsState()
    {
        if (PlayerPrefs.HasKey("isMusicOn"))
            _isMusicOn = PlayerPrefs.GetInt("isMusicOn");
        else
        {
            PlayerPrefs.SetInt("isMusicOn", 1);
        }

        if (PlayerPrefs.HasKey("isSoundOn"))
            _isSoundOn = PlayerPrefs.GetInt("isSoundOn");
        else
        {
            PlayerPrefs.SetInt("isSoundOn", 1);
        }

        ShowButtons();
    }

    void ShowButtons()
    {
        if (_isSoundOn == 1)
        {
            soundOnButton.SetActive(true);
            soundOffButton.SetActive(false);
        }
        else
        {
            soundOnButton.SetActive(false);
            soundOffButton.SetActive(true);
        }

        if (_isMusicOn == 1)
        {
            musicOnButton.SetActive(true);
            musicOffButton.SetActive(false);
        }
        else
        {
            musicOnButton.SetActive(false);
            musicOffButton.SetActive(true);
        }
    }

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    void CheckButtonState()
    {
    }

    private void OnMouseUp()
    {
        if (_isActiveSubButtons)
        {
            _isActiveSubButtons = false;
            soundOnButton.SetActive(false);
            musicOnButton.SetActive(false);
            soundOffButton.SetActive(false);
            musicOffButton.SetActive(false);
        }
        else
        {
            SetButtonsState();
            _isActiveSubButtons = true;
        }

        ResetButtonSize();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("SubSettingsButtonStateChanged", SetButtonsState);
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }
}