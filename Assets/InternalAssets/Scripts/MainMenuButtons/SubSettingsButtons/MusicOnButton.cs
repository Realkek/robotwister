﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicOnButton : Buttons
{
    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        PlayerPrefs.SetInt("isMusicOn", 0);
        ManagerEvents.CheckTriggeringEvent("SubSettingsButtonStateChanged");
        MusicPlayer.MusicAudioPlayer.CheckMusicState();
        ResetButtonSize();
    }
}