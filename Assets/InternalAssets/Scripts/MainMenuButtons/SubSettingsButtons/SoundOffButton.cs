﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOffButton : Buttons
{
    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        PlayerPrefs.SetInt("isSoundOn", 1);
        ManagerEvents.CheckTriggeringEvent("SubSettingsButtonStateChanged");
        ResetButtonSize();
    }
}