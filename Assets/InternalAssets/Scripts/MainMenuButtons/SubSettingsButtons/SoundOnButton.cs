﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SoundOnButton : Buttons
{
    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        PlayerPrefs.SetInt("isSoundOn", 0);
        ManagerEvents.CheckTriggeringEvent("SubSettingsButtonStateChanged");
        ResetButtonSize();
    }
}