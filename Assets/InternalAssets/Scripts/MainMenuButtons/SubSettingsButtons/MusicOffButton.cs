﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicOffButton : Buttons
{
    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        PlayerPrefs.SetInt("isMusicOn", 1);
        ManagerEvents.CheckTriggeringEvent("SubSettingsButtonStateChanged");
        MusicPlayer.MusicAudioPlayer.CheckMusicState();
        ResetButtonSize();
    }
}