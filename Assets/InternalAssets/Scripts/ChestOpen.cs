﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class ChestOpen : MonoBehaviour, IEventTrigger
{
    public AudioClip collectCoinsSound;
    public GameObject coinsDistributor;
    private int _chestOpenLuckyNumber;

    private ParticleSystem _coinsParticleSystem;
    private MeshRenderer _meshRenderer;
    private AudioSource _audioSource;

    private bool _isTriggeredAudioClip;

    // Start is called before the first frame update
    void Start()
    {
        _coinsParticleSystem = coinsDistributor.GetComponent<ParticleSystem>();
        _chestOpenLuckyNumber = UnityEngine.Random.Range(0, 7);
        if (_chestOpenLuckyNumber != 1)
            transform.gameObject.SetActive(false);
        _meshRenderer = GetComponent<MeshRenderer>();
        _audioSource = GetComponent<AudioSource>();
        ManagerEvents.StartListening("SubSettingsButtonStateChanged", CheckAudioButtonState);
        CheckAudioButtonState();
    }

    private void CheckAudioButtonState()
    {
        if (PlayerPrefs.GetInt("isSoundOn") == 0)
            _audioSource.mute = true;
        else if (PlayerPrefs.GetInt("isSoundOn") == 1)
        {
            _audioSource.mute = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (_isTriggeredAudioClip == false)
            {
                TriggerEvent(PlayerEventsStore.GoldCoinsReceived);
                _coinsParticleSystem.Play();
                _audioSource.PlayOneShot(collectCoinsSound);
                _isTriggeredAudioClip = true;
                transform.GetChild(0).gameObject.SetActive(false);
                transform.GetChild(1).gameObject.SetActive(false);
                _meshRenderer.enabled = false;
            }
        }
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}