﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GoldCoinsCounterText : MonoBehaviour, IEventSub
{
    private int _goldCoinsScore;

    private TextMeshProUGUI _textMeshPro;

    void Start()
    {
        _textMeshPro = GetComponent<TextMeshProUGUI>();
        _goldCoinsScore = PlayerPrefs.GetInt("GoldCoins");
        _textMeshPro.text = _goldCoinsScore.ToString();
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.GoldCoinsReceived, IncrementGoldCoinCount);
    }

    void IncrementGoldCoinCount()
    {
        _goldCoinsScore++;
        PlayerPrefs.SetInt("GoldCoins", _goldCoinsScore);
        _textMeshPro.text = _goldCoinsScore.ToString();
    }

    public void UnSubscribe()
    {
    }
}