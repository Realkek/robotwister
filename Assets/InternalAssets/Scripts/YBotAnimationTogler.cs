﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YBotAnimationTogler : MonoBehaviour, IEventSub
{
    private Animator _animator;
    private Rigidbody _rigidbody;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFirstTapped, ChangeIdleToRunning);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerJumped, ChangeRunningToJumping);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerLanded, ChangeJumpingToRunning);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerClickMissed, ChangeRunningToFalls);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFell, ChangeRunningToFalls);
    }

    void ChangeIdleToRunning()
    {
        _animator.SetBool("isRunning", true);
    }

    void ChangeRunningToJumping()
    {
        _animator.SetBool("isRunning", false);
        _animator.SetBool("isJumpingUp", true);
    }

    void ChangeJumpingToRunning()
    {
        _animator.SetBool("isRunning", true);
        _animator.SetBool("isJumpingUp", false);
    }

    void ChangeRunningToFalls()
    {
        _animator.SetBool("isRunning", false);
        _animator.SetBool("isJumpingUp", true);
        _rigidbody.useGravity = true;
    }


    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}