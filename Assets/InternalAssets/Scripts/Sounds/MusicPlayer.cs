﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public static MusicPlayer MusicAudioPlayer;
    private AudioSource _audioSource;

    private void Awake()
    {
        if (MusicAudioPlayer != null)
            Destroy(gameObject);
        else
        {
            MusicAudioPlayer = this;
        }

        _audioSource = GetComponent<AudioSource>();
        CheckMusicState();
        DontDestroyOnLoad(this);
    }

    public void CheckMusicState()
    {
        if (PlayerPrefs.GetInt("isMusicOn") == 1 && _audioSource.enabled)
            _audioSource.Play();
        else if (_audioSource.isPlaying)
        {
            _audioSource.Pause();
        }
    }
}