﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TapsPanel : MonoBehaviour, IEventTrigger, IEventSub
{
    private bool _isFirstTapped = true;

    private void Awake()
    {
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerClickMissed, DisableTapsPanel);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFell, DisableTapsPanel);
    }

    private void OnMouseDown()
    {
        if (!_isFirstTapped)
            TriggerEvent(PlayerEventsStore.PlayerTapped);
        else
        {
            TriggerEvent(PlayerEventsStore.PlayerFirstTapped);
            _isFirstTapped = false;
        }
    }

    void DisableTapsPanel()
    {
        gameObject.SetActive(false);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }

    public void UnSubscribe()
    {
    }
}