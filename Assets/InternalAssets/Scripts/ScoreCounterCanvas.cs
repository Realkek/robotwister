﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCounterCanvas : MonoBehaviour, IEventSub
{
    public GameObject scoreCounterText;

    private void Start()
    {
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerFirstTapped, ShowScoreCounterText);
    }

    void ShowScoreCounterText()
    {
        scoreCounterText.gameObject.SetActive(true);
    }

    public void UnSubscribe()
    {
    }
}