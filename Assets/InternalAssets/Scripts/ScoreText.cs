﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreText : MonoBehaviour, IEventSub, IEventTrigger
{
    private int _score;
    private TextMeshProUGUI _textMeshPro;

    private void Start()
    {
        _textMeshPro = GetComponent<TextMeshProUGUI>();
        Subscribe();
        ChangeScoreText();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.ScoreChanged, ChangeScoreText);
    }

    void ChangeScoreText()
    {
        _score = PlayerPrefs.GetInt("score");
        _textMeshPro.text = _score.ToString();
    }

    public void UnSubscribe()
    {
        ;
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}