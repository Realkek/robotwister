﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HighScoreText : MonoBehaviour, IEventSub
{
    private int _highScore;
    private TextMeshProUGUI _textMeshPro;

    void Start()
    {
        _textMeshPro = GetComponent<TextMeshProUGUI>();
        _highScore = PlayerPrefs.GetInt("HighScore");
        _textMeshPro.text = _highScore.ToString();
    }

    public void Subscribe()
    {
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}