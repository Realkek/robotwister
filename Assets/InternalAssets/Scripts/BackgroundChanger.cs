﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundChanger : MonoBehaviour
{
    [SerializeField] private List<Material> materials = new List<Material>();

    // Start is called before the first frame update
    void Start()
    {
        int randomMaterial = UnityEngine.Random.Range(0, materials.Count - 1);
        RenderSettings.skybox = materials[randomMaterial];
    }

    // Update is called once per frame
    void Update()
    {
    }
}