﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpeedBuffSphere : MonoBehaviour, IEventTrigger
{
    public AudioClip collectMoveSpeedBufSound;
    public AudioClip moveSpeedBufSound;
    public GameObject buffDistributor;
    private int _buffLuckyNumber;
    private ParticleSystem _buffParticleSystem;
    private MeshRenderer _meshRenderer;
    private AudioSource _audioSource;
    private bool _isTriggeredAudioClip;

    // Start is called before the first frame update
    void Start()
    {
        _buffParticleSystem = buffDistributor.GetComponent<ParticleSystem>();
        _buffLuckyNumber = Random.Range(0, 9);
        if (_buffLuckyNumber != 1)
            transform.gameObject.SetActive(false);
        else if (transform.parent.GetChild(1).gameObject.activeSelf)
            transform.gameObject.SetActive(false);
        _meshRenderer = GetComponent<MeshRenderer>();
        _audioSource = GetComponent<AudioSource>();
        ManagerEvents.StartListening("SubSettingsButtonStateChanged", CheckAudioButtonState);
        CheckAudioButtonState();
    }

    private void CheckAudioButtonState()
    {
        if (PlayerPrefs.GetInt("isSoundOn") == 0)
            _audioSource.mute = true;
        else if (PlayerPrefs.GetInt("isSoundOn") == 1)
        {
            _audioSource.mute = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (_isTriggeredAudioClip == false)
            {
                TriggerEvent(PlayerEventsStore.MoveSpeedBuffReceived);
                _buffParticleSystem.Play();
                _audioSource.PlayOneShot(collectMoveSpeedBufSound);
                _audioSource.PlayOneShot(moveSpeedBufSound);
                _isTriggeredAudioClip = true;
                transform.GetChild(0).gameObject.SetActive(false);
                _meshRenderer.enabled = false;
            }
        }
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}