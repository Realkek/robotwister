﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Random = System.Random;

public class Player : MonoBehaviour, IEventTrigger, IEventSub
{
    public GameObject gameOverMenuCanvas;
    public AudioClip fellSound;
    public AudioClip jumpSound2;
    public GameObject yBot;
    private Vector3 _downDirection;
    private GameObject _secondTile;
    private Vector3 _destinationPlayerPosition;
    private bool _isFlyMoving = false;
    private float _moveSpeed = 10f;
    private Vector3 _missedPlayerDestinationPosition;
    private float _tileSideDirectionCount = 0;
    private Player _player;
    private PlayerMover _playerMover;
    private AudioSource _audioSource;
    private bool _isTriggeredAudioClip;

    private void Start()
    {
        Subscribe();
        _downDirection = transform.TransformDirection(Vector3.down);
        _player = GetComponent<Player>();
        _playerMover = GetComponent<PlayerMover>();
        _audioSource = GetComponent<AudioSource>();
        CheckAudioButtonState();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerTapped, CheckWillingnessJumpPlayer);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerShifted, CheckShift);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerJumped, RiseIt);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerClickMissed, DisablePlayerObject);
        ManagerEvents.StartListening(PlayerEventsStore.PlayerJumpMissed, EnablePlayerFallOnJumpMissed);
        ManagerEvents.StartListening("SubSettingsButtonStateChanged", CheckAudioButtonState);
        ManagerEvents.StartListening(PlayerEventsStore.MoveSpeedBuffReceived, IncreaseMoveSpeed);
        ManagerEvents.StartListening(PlayerEventsStore.MoveSpeedDeBuffReceived, DecreaseMoveSpeed);
    }

    private void DecreaseMoveSpeed()
    {
        StartCoroutine(MoveSpeedDecreaser());
    }

    private void IncreaseMoveSpeed()
    {
        StartCoroutine(MoveSpeedIncreaser());
    }

    private IEnumerator MoveSpeedIncreaser()
    {
        _moveSpeed += 3f;
        yield return new WaitForSeconds(3);
        _moveSpeed -= 3f;
    }

    private IEnumerator MoveSpeedDecreaser()
    {
        _moveSpeed -= 3f;
        yield return new WaitForSeconds(3);
        _moveSpeed += 3f;
    }

    public void UnSubscribe()
    {
    }

    private void CheckAudioButtonState()
    {
        if (PlayerPrefs.GetInt("isSoundOn") == 0)
            _audioSource.mute = true;
        else if (PlayerPrefs.GetInt("isSoundOn") == 1)
        {
            _audioSource.mute = false;
        }
    }

    void DisablePlayerObject()
    {
        if (_isTriggeredAudioClip == false)
        {
            _audioSource.PlayOneShot(fellSound);
            _player.enabled = false;
            _isTriggeredAudioClip = true;
            _playerMover.enabled = false;
            StartCoroutine(GameOverMenuShower());
        }
    }

    private IEnumerator GameOverMenuShower()
    {
        yield return new WaitForSeconds(1);
        gameOverMenuCanvas.SetActive(true);
    }

    void EnablePlayerFallOnJumpMissed()
    {
        if (!_isFlyMoving && _isTriggeredAudioClip == false)
        {
            _isTriggeredAudioClip = true;
            yBot.GetComponent<Rigidbody>().useGravity = true;
            TriggerEvent(PlayerEventsStore.PlayerFell);
            _audioSource.PlayOneShot(fellSound);
            StartCoroutine(GameOverMenuShower());
        }
    }

    void CheckWillingnessJumpPlayer()
    {
        if (Physics.Raycast(transform.position, _downDirection, out RaycastHit hit, 5))
        {
            ManagerEvents.CheckTriggeringEvent(PlayerEventsStore.CurrentHitTileReceived, hit.collider.name);
        }
        else if (_isFlyMoving == false && _secondTile != null)
        {
            ManagerEvents.CheckTriggeringEvent(PlayerEventsStore.PlayerClickMissed);
            StartCoroutine(MoveToMissedTargetTile());
            if (_secondTile.transform.position.x > transform.position.x)
                _tileSideDirectionCount = 0.15f;
            else if (_secondTile.transform.position.x < transform.position.x)
                _tileSideDirectionCount = -0.15f;
        }
    }

    void CheckShift<TShiftedTile>(TShiftedTile toShiftedTile)
    {
        _secondTile = toShiftedTile as GameObject;
        _isFlyMoving = true;
    }

    void RiseIt()
    {
        _audioSource.PlayOneShot(jumpSound2);
        StartCoroutine(PlayerRiser());
    }

    private IEnumerator PlayerRiser()
    {
        int i = 0;
        while (i < 10)
        {
            i++;
            transform.Translate(0, 0.15f, 0);
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }


    void MoveToTargetTile()
    {
        transform.rotation = Quaternion.identity;
        float secondTileZPositionModifier = 0;
        switch (_secondTile.tag)
        {
            case "LongTile":
                secondTileZPositionModifier = 3;
                break;
            case "MediumTile":
                secondTileZPositionModifier = 2;
                break;
            case "ShortTile":
                secondTileZPositionModifier = 1;
                break;
        }

        Vector3 secondTilePosition = _secondTile.transform.position;
        _destinationPlayerPosition = new Vector3(secondTilePosition.x,
            secondTilePosition.y + 0.5f,
            secondTilePosition.z - secondTileZPositionModifier);
        float step = _moveSpeed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, _destinationPlayerPosition, step);
        if (Vector3.Distance(transform.position, _destinationPlayerPosition) < 0.1f)
        {
            _isFlyMoving = false;
            TriggerEvent(PlayerEventsStore.PlayerLanded);
        }
    }

    private IEnumerator MoveToMissedTargetTile()
    {
        int i = 0;
        Vector3 playerPosition = transform.position;
        while (i < 20)
        {
            i++;
            if (i < 5)
            {
                playerPosition = new Vector3(playerPosition.x + _tileSideDirectionCount, playerPosition.y,
                    playerPosition.z + 0.3f);
            }
            else
            {
                playerPosition = new Vector3(playerPosition.x + _tileSideDirectionCount, playerPosition.y,
                    playerPosition.z + 0.2f);
            }

            transform.position = playerPosition;
            yield return new WaitForSeconds(0.02f);
        }
    }

    private void Update()
    {
        if (_isFlyMoving && _secondTile != null)
        {
            MoveToTargetTile();
        }
    }
}