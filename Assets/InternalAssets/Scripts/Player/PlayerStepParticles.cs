﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStepParticles : MonoBehaviour, IEventSub
{
    private ParticleSystem _particleSystem;
    public GameObject player;

    private void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening(PlayerEventsStore.PlayerTapped, EnableStepParticles);
    }

    void EnableStepParticles()
    {
        // transform.position = player.transform.position;
        _particleSystem.Play();
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}