﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerEventsStore
{
    public static string PlayerShifted = "PlayerShifted";
    public static string PlayerFirstTapped = "PlayerFirstTapped";
    public static string PlayerTapped = "PlayerTapped";
    public static string CurrentHitTileReceived = "CurrentHitTileReceived";
    public static string PlayerJumped = "PlayerJumped";
    public static string PlayerLanded = "PlayerLanded";
    public static string PlayerClickMissed = "PlayerClickMissed";
    public static string PlayerJumpMissed = "PlayerJumpMissed";
    public static string PlayerFell = "PlayerFell";
    public static string ScoreChanged = "ScoreChanged";
    public static string GoldCoinsReceived = "GoldCoinsReceived";
    public static string MoveSpeedBuffReceived = "MoveSpeedBuffReceived";
    public static string MoveSpeedDeBuffReceived = "MoveSpeedDeBuffReceived";
    public static string TilesColorChanged = "TilesColorChanged";
}